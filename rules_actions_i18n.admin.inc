<?php

/**
 * @file
 * Creates the module's UI.
 */

/**
 * Iterates over all rules and components and saves those that are translatable.
 *
 * @return array
 *   An array containing all translatable rules along 
 *   with some extra necessary information meant for rendering.
 */
function rules_actions_i18n_fetch() {

  // Set up the query variable from the filter form.
  if (!isset($query)) {
    $query = array();
    $fields = array('label');
    
    /* This is here in case an actions filter will be implemented.
    $fields = array('label', 'action');
    */
    
    foreach ($fields as $field) {
      // If the filter form has already been used...
      if (isset($_SESSION['rules_actions_i18n_filter'][$field])) {
        $query[$field] = $_SESSION['rules_actions_i18n_filter'][$field];
      }
      // Elsewise provide a default query.
      else {
        $query = array(
          'label' => '',
          /* This is here in case an actions filter will be implemented.
          'action' => 'all',
          */
        );
      }
    }
  }
  
  // We only want regular Rules and component types 'Rule' and 'Action set'.
  $plugins = array(
    'reaction rule',
    'rule',
    'action set',
    );

  // Load all Rules adhering to our filters and plugin requirements.
  $db_query = new EntityFieldQuery();
  $db_query->entityCondition('entity_type', 'rules_config')
    ->propertyCondition('plugin', $plugins)
    ->propertyCondition('label', "%" . $query['label'] . "%", "LIKE");
  $result = $db_query->execute();
  
  // Check each Rule and save if translatable.
  if (!empty($result)) {
    $rules_load = entity_load('rules_config', array_keys($result['rules_config']));
    $rules = array();
    foreach ($rules_load as $rule) { 
      foreach (rules_actions_i18n_actions_iterator($rule) as $action) {
        // Filter out loops.
        if ($action->plugin() == 'action') {
          // Iterate over each defined action.
          foreach ($action->pluginParameterInfo() as $action_info) {
            // Save some info if one of the actions is translatable.
            if (!empty($action_info['translatable'])) {
              $rules[$rule->name]['label'] = $rule->label;
              $rules[$rule->name]['actions'][] = $action->getElementName();
              $rules[$rule->name]['link'] = 'admin/config/workflow/rules/';
              $rules[$rule->name]['link'] .= $rule->plugin() == 'action set' ? 'components' : 'reaction';
              $rules[$rule->name]['link'] .= '/manage/' . $rule->id;
            }
          }
        }
      }
    }
    return $rules;
  }
}

/**
 * Returns all actions for a given rule or component using the correct iterator.
 *
 * @param object $rule
 *   A rule object
 *
 * @return object
 *   A recursive element iterator object that may be iterated using a foreach statement.
 */
function rules_actions_i18n_actions_iterator($rule) {
  switch ($rule->plugin()) {
    case 'action set':
      return $rule->getIterator();
    break;
    default:
      return $rule->actions();
    break;
  }
}

/**
 * Creates the renderable array containing the UI table.
 *
 * @return array
 *   A renderable array
 */
function rules_actions_i18n_render() {
  // Define the table layout.
  $table = array(
    '#theme' => 'table',
    '#header' => array(t('Rule/Component label'), t('Translatable actions'), t('Operations')),
    '#rows' => array(),
  );

  if ($fetched_rules = rules_actions_i18n_fetch()) {
    foreach ($fetched_rules as $name => $row) {
    
      // Get the Rule's label.
      $label = $row['label'];
      
      // Get information about all actions and construct the info string.
      $actions = implode(', ', array_unique($row['actions']));
          
      // Construct the Operation links.
      $links = rules_actions_i18n_render_links($row['link']);
      
      // Finally add the row to our table!
      $table['#rows'][] = array($label, $actions, $links);
    }
  }
  
  return drupal_render($table);
}

/**
 * Returns the constructed operation links in a renderable array.
 *
 * @param string $link
 *   The base edit URL for a rule or action component
 *
 * @return array
 *   A renderable array
 */
function rules_actions_i18n_render_links($link) {
  $links = array(
    '#theme' => 'links',
    '#attributes' => array(
      'class' => array(
        'links', 
        'inline',
        'operations',
      ),
    ),
    '#links' => array(
      'edit' => array(
        'title' => t('Edit'),
        'href' => $link,
      ),
      'translate' => array(
        'title' => t('Translate'),
        'href' => $link . '/translate',
      ),
    ),
  );
  
  return drupal_render($links);
}


/**
 *  Admin UI screen.
 *  Consists of a filter form and a query table.
 *
 * @return string
 *    A HTML string ready for output.
 */
function rules_actions_i18n_admin_screen() {
  $form = drupal_get_form('rules_actions_i18n_filter_form');
  $output = render($form);
  $output .= rules_actions_i18n_render();
  return $output;
}

/**
 * Creates the available filters for the UI.
 *
 * @return array
 *    An array containing the available filters
 */
function rules_actions_i18n_filters() {
  $filters = array();

  $filters['label'] = array(
    'title' => t('Label contains'),
    'description' => t('Leave blank to show all rules. The search is case insensitive.'),
  );

  /* This is here in case an actions filter will be implemented.
  // Get translatable action types
  $action_all = array('all' => t('All'));
  $action_types = array('1' => 'mail', '2' => 'breadcrumb_set');
 
  $filters['action'] = array(
    'title' => t('Action is'),
    'options' => array_merge($action_all, $action_types),
  );
  */

  return $filters;
}

/**
 * Creates the filter form.
 *
 * @return array
 *    An renderable form array
 */
function rules_actions_i18n_filter_form() {
  $filters = rules_actions_i18n_filters();

  foreach ($filters as $key => $filter) {
    // Special case for 'string' filter.
    if ($key == 'label') {
      $form['filter']['label'] = array(
        '#type' => 'textfield',
        '#title' => $filter['title'],
        '#description' => $filter['description'],
      );
    }
    else {
      $form['filter'][$key] = array(
        '#title' => $filter['title'],
        '#type' => 'select',
        '#empty_value' => 'all',
        '#empty_option' => $filter['options']['all'],
        '#size' => 0,
        '#options' => $filter['options'],
      );
    }
    if (!empty($_SESSION['rules_actions_i18n_filter'][$key])) {
      $form['filter'][$key]['#default_value'] = $_SESSION['rules_actions_i18n_filter'][$key];
    }
   }
   
   /* This is here in case an actions filter will be implemented.
   $form['actions'] = array(
     '#type' => 'actions',
     '#attributes' => array('class' => array('container-inline')),
   );
   */
   
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Filter'),
  );
  if (!empty($_SESSION['rules_actions_i18n_filter'])) {
    $form['actions']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset'),
    );
  }
  return $form;
}

/**
 * Validates the filter form.
 */
function rules_actions_i18n_filter_form_validate($form, &$form_state) {
  if ($form_state['values']['op'] == t('Filter') && empty($form_state['values']['label']) && empty($form_state['values']['action'])) {
    form_set_error('type', t('You must select something to filter by.'));
  }
}

/**
 * Processes the filter form.
 */
function rules_actions_i18n_filter_form_submit($form, &$form_state) {
  $op = $form_state['values']['op'];
  $filters = rules_actions_i18n_filters();
  switch ($op) {
    case t('Filter'):
      foreach ($filters as $name => $filter) {
        if (isset($form_state['values'][$name])) {
          $_SESSION['rules_actions_i18n_filter'][$name] = $form_state['values'][$name];
        }
      }
      break;
    case t('Reset'):
      $_SESSION['rules_actions_i18n_filter'] = array();
      break;
  }
}